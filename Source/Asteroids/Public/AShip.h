// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystemInterface.h"
#include "EnhancedInputSubsystems.h"
#include "Components/SceneComponent.h"
#include "AShip.generated.h"

UCLASS()
class ASTEROIDS_API AAShip : public ACharacter
{
	GENERATED_BODY()

public:
	AAShip();

protected:
	virtual void BeginPlay() override;

	void AddInputMappingContext();
	void TurnShip(const FInputActionValue& Value);
	void Attack();

	UPROPERTY(VisibleAnywhere)
	USpringArmComponent* SpringArm;

	UPROPERTY(VisibleAnywhere)
	UCameraComponent* Camera;

	UPROPERTY(VisibleAnywhere)
	USceneComponent* ProjectileFirePoint;

	UPROPERTY(EditAnywhere)
	UInputMappingContext* InputMappingContext;

	UPROPERTY(EditAnywhere)
	UInputAction* TurnInputAction;

	UPROPERTY(EditAnywhere)
	UInputAction* AttackInputAction;

	UPROPERTY(EditAnywhere)
	TSubclassOf<AActor> Projectile;

public:	
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};
