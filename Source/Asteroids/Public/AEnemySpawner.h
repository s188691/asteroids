// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AEnemySpawner.generated.h"

UCLASS()
class ASTEROIDS_API AAEnemySpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	AAEnemySpawner();

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	TSubclassOf<AActor> Enemy;

	UPROPERTY(EditAnywhere)
	float TimeBetweenSpawn = 5.0f;

	UPROPERTY(EditAnywhere)
	float SpawnTimer = 0.0f;

	UPROPERTY(EditAnywhere)
	TArray<FVector> SpawnPoints; 

public:	
	virtual void Tick(float DeltaTime) override;

};
