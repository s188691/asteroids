// Fill out your copyright notice in the Description page of Project Settings.


#include "AEnemySpawner.h"

AAEnemySpawner::AAEnemySpawner()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AAEnemySpawner::BeginPlay()
{
	Super::BeginPlay();
	
	if (SpawnPoints.Num() == 0)
	{
		UE_LOG(LogTemp, Error, TEXT("Spawn points are not defined!"));
	}
}

void AAEnemySpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (SpawnTimer >= TimeBetweenSpawn)
	{
		int32 index = FMath::RandRange(0, SpawnPoints.Num() - 1);
		FVector spawnPoint = SpawnPoints[index];

		FTransform SpawnTransform = FTransform(GetActorRotation(), spawnPoint);
		FActorSpawnParameters SpawnParams;
		GetWorld()->SpawnActor<AActor>(Enemy, SpawnTransform, SpawnParams);
		// TODO: Fix - Warning: SpawnActor failed because of collision at the spawn location [X=0.000 Y=1000.000 Z=0.000] for [BP_Enemy_C]

		SpawnTimer = 0.0f;
	}
	else
	{
		SpawnTimer += DeltaTime;
	}
}

