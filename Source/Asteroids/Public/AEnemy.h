// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "AEnemy.generated.h"

UCLASS()
class ASTEROIDS_API AAEnemy : public APawn
{
	GENERATED_BODY()
	
public:	
	AAEnemy();

	UPROPERTY(VisibleAnywhere)
	int Health = 100;

	UPROPERTY(VisibleAnywhere)
	int MaxHealth = 100;

	UPROPERTY(VisibleAnywhere)
	float ShipSpeed = 50.0f;

	UPROPERTY(VisibleAnywhere)
	int DamageAmount = 20;

	UPROPERTY(EditAnywhere)
	UBoxComponent* CollisionComponent;

	AActor* Ship;

	void TakeDamage();
	bool IsDead();
	void Dead();
	void MoveTowardsActor(AActor* TargetActor);

	UFUNCTION()
	void OverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

};
