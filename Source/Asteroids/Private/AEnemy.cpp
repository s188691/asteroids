// Fill out your copyright notice in the Description page of Project Settings.


#include "AEnemy.h"
#include "Kismet/GameplayStatics.h"
#include "AShip.h"


AAEnemy::AAEnemy()
{
	PrimaryActorTick.bCanEverTick = true;

	CollisionComponent = CreateDefaultSubobject<UBoxComponent>("BoxCollision");
	CollisionComponent->SetupAttachment(RootComponent);
}

void AAEnemy::BeginPlay()
{
	Super::BeginPlay();
	
	CollisionComponent->OnComponentBeginOverlap.AddDynamic(this, &AAEnemy::OverlapBegin);

	TSubclassOf<AAShip> TargetClass = AAShip::StaticClass();
	Ship = UGameplayStatics::GetActorOfClass(GetWorld(), TargetClass);
	MoveTowardsActor(Ship);
}

void AAEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AAEnemy::TakeDamage()
{
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Damage taken"));

	Health -= DamageAmount;
	if (IsDead())
		Dead();
}

bool AAEnemy::IsDead()
{
	if (Health <= 0)
		return true;
	else
		return false;
}

void AAEnemy::Dead()
{
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Enemy destroyed"));
	Destroy();
}

void AAEnemy::MoveTowardsActor(AActor* TargetActor)
{
	FVector TargetLocation = TargetActor->GetActorLocation();
	FVector Direction = (TargetLocation - GetActorLocation()).GetSafeNormal();
	AddMovementInput(Direction, ShipSpeed);
}

void AAEnemy::OverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	TakeDamage();
}
