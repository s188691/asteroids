// Fill out your copyright notice in the Description page of Project Settings.


#include "AShip.h"
#include "Kismet/GameplayStatics.h"

AAShip::AAShip()
{
	PrimaryActorTick.bCanEverTick = true;

	SpringArm = CreateDefaultSubobject<USpringArmComponent>("SpringArm");
	SpringArm->SetupAttachment(RootComponent);

	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
	Camera->SetupAttachment(SpringArm);

	ProjectileFirePoint = CreateDefaultSubobject<USceneComponent>("ProjectileFirePoint");
	ProjectileFirePoint->SetupAttachment(RootComponent);
}

void AAShip::BeginPlay()
{
	Super::BeginPlay();

	AddInputMappingContext();
}

void AAShip::AddInputMappingContext()
{
	APlayerController* PlayerController = Cast<APlayerController>(GetController());
	if (PlayerController)
	{
		UEnhancedInputLocalPlayerSubsystem* EnhancedInputSubsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer());
		if (EnhancedInputSubsystem)
		{
			EnhancedInputSubsystem->AddMappingContext(InputMappingContext, 0);
		}
	}
}

void AAShip::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AAShip::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(PlayerInputComponent);
	if (EnhancedInputComponent)
	{
		EnhancedInputComponent->BindAction(TurnInputAction, ETriggerEvent::Triggered, this, &AAShip::TurnShip);
		EnhancedInputComponent->BindAction(AttackInputAction, ETriggerEvent::Started, this, &AAShip::Attack);
	}
}

void AAShip::TurnShip(const FInputActionValue& Value)
{
	FVector2D InputVector = Value.Get<FInputActionValue::Axis2D>();
	APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	PlayerPawn->AddControllerYawInput(InputVector.Y);
}

void AAShip::Attack()
{
	FTransform SpawnTransform = FTransform(ProjectileFirePoint->GetComponentRotation(), ProjectileFirePoint->GetComponentLocation());
	FActorSpawnParameters SpawnParams;
	GetWorld()->SpawnActor<AActor>(Projectile, SpawnTransform, SpawnParams);
}

